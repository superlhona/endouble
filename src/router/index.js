import Vue from 'vue';
import Router from 'vue-router';
import JobDescription from '@/components/JobDescription';
import JobDescriptionForm from '@/components/JobDescriptionForm';
import JobApplied from '@/components/JobApplied';

Vue.use(Router);

export default new Router({
    routes: [
        {
            path: '/',
            name: 'JobDescription',
            component: JobDescription,
        },
        {
            path: '/apply',
            name: 'JobDescriptionForm',
            component: JobDescriptionForm,
        },
        {
            path: '/thankyou',
            name: 'JobApplied',
            component: JobApplied,
        },
    ],
    scrollBehavior(to, from, savedPosition) {
        if (savedPosition) {
            return savedPosition;
        }

        return { x: 0, y: 0 };
    },
});
