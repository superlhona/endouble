# Project Title

Endouble

## Project Description

Front-end test for Endouble by Illona Fedora. This project is using Vue CLI.

## Usage

Installing dependencies

```
$ npm install
```

Local development

```
$ npm run dev
```

## Deployment

Automatically deployed on git push to master

## Built With

* [Vue.js Templates](https://github.com/vuejs-templates/webpack) - The web framework used
* [Vue Router](https://github.com/vuejs/vue-router) - Routing
* [VeeValidate](https://github.com/baianat/vee-validate) - Form validation
* [Bootstrap 4](https://getbootstrap.com/docs/4.3/getting-started/introduction/) - The styling grid used